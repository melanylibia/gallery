import React, { useState } from 'react';
import axios from 'axios';
export default function ImageForm() {
    const [file, setFile] = useState();
    const [title, setTitle] = useState("");
    const handleChange = e => {
        setFile(e.target.files[0]);
    }
    const setValues = e => {
        setFile();
        setTitle('');
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('file', file)
        formData.append('title', title)
        await axios.post('/api/images/upload', formData)
        setValues()
    }
    return (
        <div className="row">
            <div className="col-md-4 offset-md-4">
                <div className="card">
                    <div className="card-header text-center">
                        <h3>Upload a Image File</h3>
                    </div>
                    <div className="card-body">
                        <form onSubmit={handleSubmit}>
                            <div className="input-group mb-3">
                                <div className="custom-file">
                                    <label
                                        className="custom-file-label"
                                        htmlFor="inputGroupFile02"
                                        aria-describedby="inputGroupFileAddon02"
                                    >
                                        Choose file
                                    </label>
                                    <input
                                        type="file"
                                        name="archivo"
                                        className="custom-file-input"
                                        id="inputGroupFile02"
                                        onChange={handleChange}
                                    />


                                </div>
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    name="title"
                                    value={title}
                                    placeholder="Title for an image"
                                    onChange={e => { setTitle(e.target.value) }}
                                    className="form-control"
                                />
                            </div>

                            <br />
                            <div className="form-group">
                                <button className="btn btn-success btn-block">Upload</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
