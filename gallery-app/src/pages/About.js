import React from 'react'

function About() {
    return (
        <div className='container'>
            <div className="row">
                <div className="col-md-4">
                Whenever you feel like a French knight and want to escape the sorrows of life, use this article. If you listen, eat, smell, or watch, some of the things above memories will surface.
                Save your memories here.
                </div>
            </div>
        </div>
    )
}

export default About
