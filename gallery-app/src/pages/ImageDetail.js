import React from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
export default function ImageDetail({ image }) {
  const { imageid } = image;
  const navigate = useNavigate();
  const deleteHandler = async() => {
    await axios.delete('/delete/'+imageid);
    navigate(0);
  };
  return (
    <div className="col m-3 pl-2">
      <div className="card p-2" style={{ width: '18rem' }}>
        <img src={image.url} className="card-img-top" />
        <div className="card-body">
          <h5 className="card-title">{image.title}</h5>
          <button onClick={deleteHandler} className="btn btn-primary">Delete</button>
        </div>
      </div>
    </div>
  )
}
