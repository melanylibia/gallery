import React, { useEffect, useState } from 'react'
import axios from 'axios';
import ImageDetail from './ImageDetail';
import uuid from 'react-uuid';
export default function ImageGallery() {
    const [images, setImages] = useState([])
    useEffect(() => {
        (async () => {
            const res = await axios.get('/api/images')
            setImages(res.data);
        })()
    }, []);

    return (

        <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            {images.map((image) =>
                <ImageDetail image={image} key={uuid()} />
            )}
        </div>
    )
}
