import ImageGallery from './pages/ImageGallery';
import React from 'react';
import ImageForm from './pages/ImageForm';
import About from './pages/About';
import Navbar from './components/Navbar';
import './App.css';
import Footer from './components/Footer'
import {
  Routes,
  Route
} from "react-router-dom";

function App() {
  return (
    <div>
      <Navbar />
      <div className='container p-4'>
        <Routes>
          <Route path="/" element={<ImageGallery />} />
          <Route path="/upload" element={<ImageForm />} />
          <Route path="/about" element={<About />} />
        </Routes>

      </div>
      <Footer />
    </div>

  );
}

export default App;
