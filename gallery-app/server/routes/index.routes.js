

import { Router } from "express";
import AWS from "aws-sdk";

const router = Router();


router.get('/', (req, res) => {
    return res.json({
        msg: 'Welcome to API'
    })
});

export default router;