import { Router } from "express";
import AWS from "aws-sdk";
import config from "../config";
const pool = require('../database');
const router = Router();
const spacesEndpoint = new AWS.Endpoint(config.Endpoint);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
});
// Upload an image
router.post("/api/images/upload", async (req, res) => {
  // if there is no file returns
  if (!req.files) return res.json({ msg: "no file" });
  const file = req.files.file;
  try {
    // S3 Upload
    const uploadedObject = await s3
      .putObject({
        ACL: "public-read",
        Bucket: config.BucketName,
        Key: file.name,
        Body: file.data,
      })
      .promise();
    const urlImage = `https://${config.BucketName}.${config.Endpoint}/${file.name}`;

    // save the url in databae
    const newFile =
    {
      url: urlImage,
      imagekey: file.name,
      title: req.body.title,
    }

    await pool.query('INSERT INTO Image set ?', [newFile]);

    return res.json(newFile);

  } catch (error) {
    res.send(error);
  }
});
// Get full images
router.get("/api/images",
  async (req, res) => {
    const images = await pool.query('SELECT * FROM Image');
    res.json(images);
  }
);

//GET AN IMAGE
router.get("/api/images/:id", async (req, res) => {
  const { id } = req.params;
  const image = await pool.query('SELECT * FROM Image WHERE imageid=?', [id]);
  res.json(image[0]);
});
//DELETE AN IMAGE
router.delete('/delete/:id', async (req, res) => {
  const { id } = req.params;
  const currimage = await pool.query('SELECT * FROM Image WHERE imageid=?', [id]);
  if (!currimage[0]){
    return res.json({"msg":"Not Found"})
  }
  await pool.query("DELETE FROM Image WHERE imageid= ?", [id]);
  await s3
    .deleteObject({
        Bucket: config.BucketName,
        Key: currimage[0].imagekey,
      })
      .promise();  
  res.json(currimage[0]);
});

export default router;
